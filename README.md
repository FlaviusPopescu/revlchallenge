### Flavius' Revl Challenge Submission

This app implements the base requirements of the challenge. The source is structured in these major packages:

* **data**: includes a repository class that can be used to abstract away the details of fetching images from the network, as well as the entities and the interface for calling the API

* **feature**: feature sub-packages for the grid-view and the detail/fullscreen view. The grid-view feature defines a contract for the presenter-view interactions

* **base**: abstract view and presenter classes to handle MVP lifecycle.

* **di**: the component used in the app with its API module that provides the implementation of the Retrofit service. Later, this service is used in the repository (mentioned above), injected on construction. The repository is injected in the presenters of the feature

### Notes

Future work:

* the grid presenter has a dependency on a repository. That can be mocked and a jUnit/Mockito test can be written to cover the presenter's business logic, including: offset feature, loading images when lifecycle is triggered, calling the right view methods when the user interacts with the grid items

* the images in the grid are too large and loading them is not smooth. Smaller versions of the images could be requested from the API. Other improvements: prefetching, smarter threading and cancelling requests, better caching, resizing to fit.

* add input field for query term: the presenter already relies on a query variable, the control just needs surfacing to the user.

* responsive grid

* the bonus requirements

### Screenshots

The grid view:

![Screen Shot 2017-05-22 at 8.09.10 PM.png](https://bitbucket.org/repo/EggxBx8/images/852913458-Screen%20Shot%202017-05-22%20at%208.09.10%20PM.png)

The detail/fullscreen view:

![Screen Shot 2017-05-22 at 8.28.01 PM.png](https://bitbucket.org/repo/EggxBx8/images/3181464653-Screen%20Shot%202017-05-22%20at%208.28.01%20PM.png)