package com.github.flaviuspopescu.revlchallenge.data

data class Image(val contentUrl: String)
