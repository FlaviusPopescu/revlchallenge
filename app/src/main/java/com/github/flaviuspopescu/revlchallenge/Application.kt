package com.github.flaviuspopescu.revlchallenge

import com.github.flaviuspopescu.revlchallenge.di.ApplicationComponent
import com.github.flaviuspopescu.revlchallenge.di.DaggerApplicationComponent

class Application : android.app.Application() {

    companion object {
        lateinit var graph: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        graph = DaggerApplicationComponent.builder().build()
    }
}
