package com.github.flaviuspopescu.revlchallenge.data.entity

import com.github.flaviuspopescu.revlchallenge.data.Image

data class ImageSearchResponse(val _type: String, val value: Array<Image>)
