package com.github.flaviuspopescu.revlchallenge.feature.search

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.github.flaviuspopescu.revlchallenge.Application
import com.github.flaviuspopescu.revlchallenge.R
import com.github.flaviuspopescu.revlchallenge.base.BaseActivity
import com.github.flaviuspopescu.revlchallenge.base.BasePresenter
import com.github.flaviuspopescu.revlchallenge.data.Image
import com.github.flaviuspopescu.revlchallenge.feature.searchfullscreen.SearchFullscreenView
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchActivity : BaseActivity<SearchContract.View>(), SearchContract.View {
    @Inject lateinit var searchPresenter: SearchPresenter
    lateinit var searchAdapter: SearchGridAdapter

    override fun getPresenter(): BasePresenter<SearchContract.View> {
        return searchPresenter
    }

    override fun getView(): SearchContract.View {
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Application.graph.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        searchAdapter = SearchGridAdapter(this)
        imageGrid.adapter = searchAdapter
        imageGrid.layoutManager = GridLayoutManager(this, 4)

        imageGrid.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val visibleItems = recyclerView.layoutManager.childCount
                val totalItems = recyclerView.layoutManager.itemCount
                val firstVisibleItem = (recyclerView.layoutManager as GridLayoutManager).findFirstVisibleItemPosition()
                if (firstVisibleItem + visibleItems >= totalItems) {
                    searchPresenter.onLoadMore()
                }
            }
        })
    }

    override fun showImages(images: ArrayList<Image>) {
        searchAdapter.images = images
        searchAdapter.notifyDataSetChanged()
    }

    override fun addMoreImages(images: ArrayList<Image>) {
        val oldCount = searchAdapter.images.size
        searchAdapter.images.addAll(images)
        searchAdapter.notifyItemRangeInserted(oldCount, images.size)
    }

    override fun onImageClicked(image: Image) {
        searchPresenter.onImageClicked(image)
    }

    override fun onImageLongClicked(image: Image) {
        searchPresenter.onImageLongClicked(image)
    }

    override fun showFullScreenImage(image: Image) {
        val searchFullscreenView = SearchFullscreenView(image)
        searchFullscreenView.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme)
        searchFullscreenView.show(supportFragmentManager, "fullscreen_view")
    }

    override fun copyToClipboard(contentUrl: String) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("content_url", contentUrl)
        clipboard.primaryClip = clip
        Toast.makeText(this, getString(R.string.copied_url, contentUrl), Toast.LENGTH_LONG).show()
    }
}
