package com.github.flaviuspopescu.revlchallenge.data

import com.github.flaviuspopescu.revlchallenge.data.entity.ImageSearchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface ImageService {

    @Headers("Ocp-Apim-Subscription-Key: 5db7a77b19d943f2a4cce83ce6b59502")
    @GET("search?mkt=en-us&safeSearch=Moderate")
    fun getImages(@Query("q") query: String,
                  @Query("count") count: Int,
                  @Query("offset") offset: Int): Observable<ImageSearchResponse>
}
