package com.github.flaviuspopescu.revlchallenge.feature.search

import com.github.flaviuspopescu.revlchallenge.base.BasePresenter
import com.github.flaviuspopescu.revlchallenge.base.BaseView
import com.github.flaviuspopescu.revlchallenge.data.Image

interface SearchContract {
    interface View : BaseView {
        fun showImages(images: ArrayList<Image>)
        fun addMoreImages(images: ArrayList<Image>)
        fun onImageClicked(image: Image)
        fun showFullScreenImage(image: Image)
        fun onImageLongClicked(image: Image)
        fun copyToClipboard(contentUrl: String)
    }

    interface Presenter : BasePresenter<View> {
        fun onLoadMore()
        fun onImageClicked(image: Image)
        fun onImageLongClicked(image: Image)
    }
}