package com.github.flaviuspopescu.revlchallenge.di

import com.github.flaviuspopescu.revlchallenge.data.ImageService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module class ApiModule {

    @Provides fun providesImageService():ImageService {
        return Retrofit.Builder().baseUrl("https://api.cognitive.microsoft.com/bing/v5.0/images/")
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ImageService::class.java)
    }
}
