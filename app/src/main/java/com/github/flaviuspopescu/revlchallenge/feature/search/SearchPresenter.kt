package com.github.flaviuspopescu.revlchallenge.feature.search

import android.util.Log
import com.github.flaviuspopescu.revlchallenge.data.Image
import com.github.flaviuspopescu.revlchallenge.data.ImageRepository
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Presenter for the Search feature. It requires a repository dependency in order to fetch images,
 * as well as its view, to display results.
 */
class SearchPresenter @Inject constructor(
        val imageRepository: ImageRepository) : SearchContract.Presenter {

    val TAG = "SearchPresenter"

    lateinit var view: SearchContract.View

    var offset = 0
    var query: String = "skiing" // use a default query term for now

    override fun onBind(view: SearchContract.View) {
        this.view = view
    }

    override fun onStart() {
        loadImages()
    }

    private fun loadImages() {
        imageRepository.loadImages(query, offset).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : Observer<ArrayList<Image>> {
                    override fun onSubscribe(disposable: Disposable) {}

                    override fun onNext(images: ArrayList<Image>) {
                        view.showImages(images)
                        offset = images.size
                    }

                    override fun onError(throwable: Throwable) {
                        Log.e(TAG, "Could not fetch images!", throwable)
                    }

                    override fun onComplete() {}
                })
    }

    override fun onLoadMore() {
        imageRepository.loadImages(query, offset).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : Observer<ArrayList<Image>> {
                    override fun onComplete() {}

                    override fun onNext(images: ArrayList<Image>) {
                        offset += images.size
                        view.addMoreImages(images)
                    }

                    override fun onError(throwable: Throwable?) {
                        Log.e(TAG, "Could not fetch images!", throwable)
                    }

                    override fun onSubscribe(p0: Disposable?) {}
                })
    }

    override fun onImageClicked(image: Image) {
        // any business logic, later?
        view.showFullScreenImage(image)
    }

    override fun onImageLongClicked(image: Image) {
        // any other business logic, later?
        view.copyToClipboard(image.contentUrl)
    }
}
