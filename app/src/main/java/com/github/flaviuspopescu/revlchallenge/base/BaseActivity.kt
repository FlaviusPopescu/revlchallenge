package com.github.flaviuspopescu.revlchallenge.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<T : BaseView> : AppCompatActivity() {
    abstract fun getPresenter(): BasePresenter<T>
    abstract fun getView(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getPresenter().onBind(getView())
    }

    override fun onStart() {
        super.onStart()
        getPresenter().onStart()
    }
}
