package com.github.flaviuspopescu.revlchallenge.feature.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.github.flaviuspopescu.revlchallenge.R
import com.github.flaviuspopescu.revlchallenge.data.Image
import kotlinx.android.synthetic.main.search_grid_item.view.*

class SearchGridAdapter(val view: SearchContract.View) : RecyclerView.Adapter<SearchGridAdapter.ViewHolder>() {

    var images: MutableList<Image> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.search_grid_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(holder.itemView.context)
                .load(images[position].contentUrl)
                .centerCrop()
                .into(holder.itemImage)

        holder.itemView.setOnClickListener { view.onImageClicked(images[position]) }
        holder.itemView.setOnLongClickListener {
            view.onImageLongClicked(images[position])
            true
        }
    }

    override fun getItemCount(): Int {
        return images.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemImage: ImageView = itemView.itemImage
    }
}
