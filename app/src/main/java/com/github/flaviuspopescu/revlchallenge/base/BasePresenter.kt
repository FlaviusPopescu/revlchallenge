package com.github.flaviuspopescu.revlchallenge.base

/**
 * Base presenter class contains lifecycle methods.
 */
interface BasePresenter<T : BaseView> {
    /**
     * In this simple app, called when presenter is ready to load data / do work, e.g. activity on start
     */
    fun onStart()

    /**
     * Called when Android component (activity) is created
     */
    fun onBind(view: T)
}