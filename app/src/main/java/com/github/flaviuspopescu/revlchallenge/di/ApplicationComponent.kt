package com.github.flaviuspopescu.revlchallenge.di

import com.github.flaviuspopescu.revlchallenge.feature.search.SearchActivity
import dagger.Component

@Component(modules = arrayOf(ApiModule::class))
interface ApplicationComponent {
    fun inject(activity: SearchActivity)
}
