package com.github.flaviuspopescu.revlchallenge.feature.searchfullscreen

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.github.flaviuspopescu.revlchallenge.R
import com.github.flaviuspopescu.revlchallenge.data.Image
import kotlinx.android.synthetic.main.search_fullscreen.*

class SearchFullscreenView(val image: Image) : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_fullscreen, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Glide.with(view.context)
                .load(image.contentUrl)
                .into(image_fullscreen)
    }
}
