package com.github.flaviuspopescu.revlchallenge.data

import io.reactivex.Observable
import javax.inject.Inject

class ImageRepository @Inject constructor(val imageService: ImageService) {
    fun loadImages(q: String, offset: Int): Observable<ArrayList<Image>> {
        return loadImages(q, 50, offset)
    }

    fun loadImages(q: String, count: Int, offset: Int): Observable<ArrayList<Image>> {
        return imageService.getImages(q, count, offset).map {
            val imageList = ArrayList<Image>()
            if (it.value.isNotEmpty()) {
                for (image in it.value) {
                    imageList.add(image)
                }
            }
            imageList
        }
    }
}
